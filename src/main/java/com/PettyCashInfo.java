package com;

/**
 * This class was automatically generated by the data modeler tool.
 */

@javax.persistence.Entity
public class PettyCashInfo implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.AUTO, generator = "PETTYCASHDETAILSDO_ID_GENERATOR")
	@javax.persistence.Id
	@javax.persistence.SequenceGenerator(name = "PETTYCASHDETAILSDO_ID_GENERATOR", sequenceName = "PETTYCASHDETAILSDO_ID_SEQ")
	private java.lang.Long id;

	@org.kie.api.definition.type.Label("Amount")
	private java.lang.Double amount;

	@org.kie.api.definition.type.Label("Reason")
	private java.lang.String reason;

	public PettyCashInfo() {
	}

	public java.lang.Long getId() {
		return this.id;
	}

	public void setId(java.lang.Long id) {
		this.id = id;
	}

	public java.lang.Double getAmount() {
		return this.amount;
	}

	public void setAmount(java.lang.Double amount) {
		this.amount = amount;
	}

	public java.lang.String getReason() {
		return this.reason;
	}

	public void setReason(java.lang.String reason) {
		this.reason = reason;
	}

	public PettyCashInfo(java.lang.Long id, java.lang.Double amount,
			java.lang.String reason) {
		this.id = id;
		this.amount = amount;
		this.reason = reason;
	}

}